CREATE TABLE llx_bibliotheque_c_book_style
(
    -- BEGIN MODULEBUILDER FIELDS
    rowid integer AUTO_INCREMENT PRIMARY KEY NOT NULL,
    code  varchar(32) DEFAULT '(BOOK)' NOT NULL,
    label varchar(128),
    active tinyint DEFAULT 1 NOT NULL
    -- END MODULEBUILDER FIELDS
)
